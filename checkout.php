<?php
$date = date("Y-m-d");
$name= $_POST['name'];
$adress= $_POST['adress'];
$message= $_POST['message'];
if ($message == NULL) $message="-";
$email= $_POST['email'];
$reguler= (int)$_POST['reguler']*20000;
if ($reguler == NULL) $reguler=0;
$Children= (int)$_POST['Children']*17000;
if ($Children == NULL) $Children=0;
$senior= (int)$_POST['senior']*25000;
if ($senior == NULL) $senior=0;
$Styling= (int)$_POST['Styling']*25000;
if ($Styling == NULL) $Styling=0;
$senior2= (int)$_POST['senior2']*30000;
if ($senior2 == NULL) $senior2=0;
$polish= (int)$_POST['polish']*40000;
if ($polish == NULL) $polish=0;
$pomade= (int)$_POST['pomade']*20000;
if ($pomade == NULL) $pomade=0;
$hairtonic= (int)$_POST['hairtonic']*20000;
if ($hairtonic == NULL) $hairtonic=0;
$vitamin= (int)$_POST['vitamin']*3000;
if ($vitamin == NULL) $vitamin=0;
$hairoil= (int)$_POST['hairoil']*20000;
if ($hairoil == NULL) $hairoil=0;
$total = $reguler+$Children+$senior+$Styling+$senior2+$polish+$pomade+$hairtonic+$vitamin+$hairoil;
$fp = fopen("struk.txt", "a+");
fputs($fp, "$date|$name|$email|$adress|$reguler|$Children|$senior|$Styling|$senior2|$polish|$pomade|$hairtonic|$vitamin|$hairoil|$total|$message\n");
fclose($fp);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout Sucess Page</title>
    <link rel="stylesheet" href="./dist/output.css">
</head>
<body>
  <div class="absolute top-0 right-0 bottom-0 left-0 w-full h-full overflow-hidden bg-fixed">
    <div class="flex justify-center items-center h-full">
      <div class="text-dark">
        <h2 class="font-semibold text-4xl mb-4">Successfully</h2>
        <h4 class="font-semibold text-xl mb-6">Ordered</h4>
        <a class="inline-block px-7 py-3 mb-1 border-2 border-primary text-primary font-medium text-sm leading-snug uppercase rounded hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out" href="./index.html" role="button">Back To Home</a>
        <a class="inline-block px-7 py-3 mb-1 border-2 border-primary text-primary font-medium text-sm leading-snug uppercase rounded hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out" href="./lihat.php" role="button">Purchase History</a>
      </div>
    </div>
  </div>
</div>
</body>
</html>